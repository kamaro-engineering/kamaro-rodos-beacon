#include "ping_responder.hpp"

void PingResponder::put(ping &received_ping) {
    uint32_t sender_id = received_ping.sender_id;
    uint32_t receiver_id = received_ping.receiver_id;
    uint32_t hop_count = received_ping.hop_count;
    uint64_t ping_data = received_ping.data;
    if (receiver_id == this->id) {

        PRINTF("I was pinged. Sender: %u, Receiver: %u, HopCount: %u, Data: %u.\n", sender_id, receiver_id, hop_count, ping_data);

        if (hop_count > 0) {

            ping response;
            response.sender_id = this->id;
            response.receiver_id = sender_id;
            response.hop_count = hop_count - 1;
            response.data = ping_data;

            ping_topic.publish(response);
        }
    }
}
