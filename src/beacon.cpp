#include "beacon.hpp"

void Beacon::run() {
    imalive data;
    data.id = this-> id;
    TIME_LOOP(0, 100*MILLISECONDS) {
        PRINTF("Beacon id %u publishing.\n", this->id);
        imalive_topic.publish(data);
    }
}
