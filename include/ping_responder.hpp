#pragma once
#include "rodos.h"

#include "ping_topic.hpp"

class PingResponder : public SubscriberReceiver<ping> {

    private:

    uint32_t id;

    public:

    PingResponder(uint32_t id):
    SubscriberReceiver<ping>(ping_topic, "ping-responder"),
    id(id)
    {}
    
    void put(ping &received_ping);
};
