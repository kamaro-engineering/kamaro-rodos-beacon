#pragma once
#include "rodos.h"

#include "imalive_topic.hpp"

class Beacon : public StaticThread<2048> {

    private:

    uint32_t id;

    public:

    Beacon(uint32_t id):
    StaticThread<2048>("beacon", 20),
    id(id)
    {}
    
    void init() {}

    void run();
};
